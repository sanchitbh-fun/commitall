<?php

$base = 'E:\Dropbox\projects';

require(__DIR__ . '/vendor/autoload.php');

$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($base));
$iterator->setMaxDepth(2);
$seen = [];

foreach ($iterator as $file) {
    $dir = $file->getRealpath();
    if ($file->isDir() && (substr_count($dir, DIRECTORY_SEPARATOR) == 4) && @!$seen[$dir]++) {
		chdir($dir);
		$out = `commit.bat 2>&1`;

		if (!preg_match('/nothing to commit/i', $out)) {
			echo $out, "\n\n";
		} else {
			print "$dir: done\n";
		}
	}
}
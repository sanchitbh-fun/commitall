<?php


$base = 'E:\Dropbox\projects';

use Buzz\Message\MessageInterface;

require('vendor/autoload.php');

$bitbucket = getBitBucket();
$iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($base));
$iterator->setMaxDepth(2);
$seen = [];

foreach ($iterator as $file) {
    $dir = $file->getRealpath();
    if ($file->isDir() && (substr_count($dir, DIRECTORY_SEPARATOR) == 4) && !$seen[$dir]++) {
        echo "DIR: ", $dir, "\n";
        chdir($dir);

        $remote = `git remote get-url origin --all`;

        if (preg_match('/git\@bitbucket.org:/', $remote)) {
            $repo = trim($remote);
        } else {
            $repo = getRepo($dir);
        }

        if (!is_file("$dir/.gitignore")) {
            echo "INIT GITIGNORE: ", $dir, "\n";
            file_put_contents("$dir/.gitignore", join("\n", [
                'vendor', 'node_modules', 'bower_components', '.idea', '.build', 'db_data', '_metadata',
            ]));
        }

        if (!is_dir("$dir/.git")) {
            echo "INIT GIT: ", $dir, "\n";

            run('git init');
            run('git remote add origin %s', $repo);
        }

        $status = `git status`;

        if (!preg_match('/nothing to commit/', $status)) {
            run('git add -A .');
            run('git commit -m "commit: %s"', date('d-M-Y'));
            run('git push -u origin master');
        } else {
            echo "Nothing to commit!", "\n";
        }
    }
}


function getBitBucket() {
    $bitbucket = new \Bitbucket\API\Api();
    $listener = new \Bitbucket\API\Http\Listener\OAuth2Listener([
        'client_id'     => '8TX6VXjQwDBJ2vqChm',
        'client_secret' => '2J9nJHh58FRVuRynKBpsrEUTN8xN6MFd',
    ]);

    $bitbucket->getClient()->addListener($listener);

    return $bitbucket;
}

function getRepo($path) {
    global $bitbucket;

    $decode = function (MessageInterface $response) {
        $content = json_decode($response->getContent());

        if ($content->type == 'error') {
            echo "ERROR: ", $content->error->message, "\n";
            return FALSE;
        }

        return $content;
    };

    list($dir, $name) = [basename(dirname($path)), basename($path)];
    $repositories = $bitbucket->api('Repositories')->getClient()->setApiVersion('2.0');
    $repository = "repositories/sanchitbh-$dir/$name";
    $response = $repositories->get($repository);

    if (!($content = $decode($response))) {
        echo "CREATING: ", $repository, "\n";
        $response = $repositories->post($repository, ['scm' => 'git', 'is_private' => TRUE]);
        $content = $decode($response);
    }

    return sprintf("git@bitbucket.org:%s.git", $content->full_name);
}

function run(...$args) {
    $run = call_user_func_array('sprintf', $args);
    echo "CMD: ", $run, "\n";
    system($run);
}